# API Clicks

## Solution

- Programming language: Python
- Python framework: Sanic
- DB: MySQL
- Docker
- pytest for tests

## SQL

In init.sql you can see how i created click_log table. Becuase requests will mostly contain search by campaign
and timestamp i've put indexes on those fields.

## Getting started

There is two ways to run this service. With with docker for develop environment or on centos 7 machine for production.

## Running on windows with docker

#Using git bash:

In project directory run the following commands:

- clone project
- winpty docker-compose up -d
to check state of docker containers:
- winpty docker-compose ps
Run following commands to check if everything is ok in mysql container:
- winpty docker-compose exec mysql bash
- mysql -u veljko -p (this command exec in mysql container, password is in docker-compose.yml file)
- use veljko;
- show tables; (because of init.sql there is supposed to be table click_log)
- select * from click_log; (there supposed to be 14 rows because we inserted it with init.sql)

After checking that sql is ok, run the following commands in new git bash tab to wake up the Sanic beast:
- winpty docker-compose exec horses bash
- python3.6 setup.py develop (to prepare environment and install all requirements)
- python3.6 -m horses (to start Sanic app)

After this You can test app with postman (remember to use token for auth, informations about it is in environment file)

To run unit tests, stop Sanic app and in horses container run pytest -vvv

## Running on centos 7 machine:

- clone project in /opt directory
- from project directory copy horses.service to /etc/systemd/system/horses.service
- run command systemctl daemon-reload
- change permissions for script startHorses.sh in /opt/api-clicks/horses/ to 755
- in environment file change all informations about mysql db
- run command systemctl start horses.service
- test application api, example: 
- curl -H "X-My-App-Auth-Token: <TOKEN FROM ENVIRONMENT FILE" localhost:9002/get_clicks_per_campaign/campaign=4510461