#!/bin/bash

python3.6 /opt/api-clicks/horses/setup.py develop
pip3 install -r /opt/api-clicks/horses/requirements.txt
gunicorn horses.server:app --bind 0.0.0.0:9002 --worker-class sanic.worker.GunicornWorker -w 4 --threads 1 --timeout 600 --max-requests 20 --max-requests-jitter 10
