import re
from setuptools import find_packages, setup


# Read package metadata from a Python module.
meta = {}
with open('horses/__meta__.py') as f:
    exec(f.read(), meta)


# Process pip requirements into setuptools dependencies.
requirements_files = ['requirements.txt']
install_requires = []
dependency_links = []

for f in map(open, requirements_files):
    for line in filter(None, map(str.strip, f)):  # Ignore whitespace.
        # Enqueue referenced requirements files for processing.
        r_pattern = r"^(-r|--requirement) "
        if re.match(r_pattern, line):
            file = re.sub(r_pattern, '', line)  # Remove option part.
            if file not in requirements_files:
                requirements_files.append(file)
            continue

        # Add editable requirements as setuptools dependency links.
        e_pattern = r"^(-e|--editable) "
        if re.match(e_pattern, line):
            link = re.sub(e_pattern, '', line)  # Remove option part.
            dependency_links.append(link)
            try:
                egg = re.search(r"#egg=(\S+)", link)[1]
            except TypeError as e:
                raise Exception(f"Dependency {link} must specify egg") from e
            install_requires.append(egg)
            continue

        # Add basic requirements directly, ignoring options and comments.
        if not re.match(r"^[-#]", line):
            install_requires.append(line)


setup(
    name=meta['__name__'],
    version=meta['__version__'],
    author=meta['__author__'],
    description=meta['__description__'],
    packages=find_packages(),
    install_requires=install_requires,
    dependency_links=dependency_links,
)
