import os

from horses.utils import envs

DEBUG = envs.getenv('DEBUG', 'false', convert_to_bool=True)
LOG_LEVEL = envs.getenv('LOG_LEVEL', 'DEBUG' if DEBUG else 'INFO')

MYSQL_HOST = envs.getenv('MYSQL_HOST', convert_to_bool=False)
MYSQL_PORT = envs.getenv('MYSQL_PORT', convert_to_bool=False)
MYSQL_USER = envs.getenv('MYSQL_USER', convert_to_bool=False)
MYSQL_PASSWORD = envs.getenv('MYSQL_PASSWORD', convert_to_bool=False)
MYSQL_DATABASE = envs.getenv('MYSQL_DATABASE', convert_to_bool=False)

APP_TOKEN = envs.getenv('APP_TOKEN', convert_to_bool=False)
