import logging
import os
import subprocess
import traceback
import tracemalloc
import asyncio
import time
import datetime
import pytz

from csv import reader

from sanic import Sanic
from sanic.response import json

from horses import settings
from horses import log
from horses.utils import envs
from horses.services import mysql_service

from sanic_token_auth import SanicTokenAuth

logger = logging.getLogger(__name__)

app = Sanic(log_config=log.CONFIG)
app.config.from_pyfile(settings.__file__)
auth = SanicTokenAuth(app, secret_key=settings.APP_TOKEN, header='X-My-App-Auth-Token')


@app.route("/health", methods=["GET"])
@auth.auth_required
async def health(request):
    """
        Health check route
    :return: 200
    """
    return json({"message": "ok"})


@app.route("/get_clicks_per_campaign/campaign=<campaign:int>", methods=['GET'])
@auth.auth_required
async def get_number_of_clicks_per_campaign(request, campaign):

    try:

        query = "select count(id) from click_log where campaign='{0}';".format(campaign)
        result = await mysql_service.get_data_for_api(query)

        return json({"Clicks": result[0]})

    except Exception as e:

        return json({"Message": e})


@app.route("/get_clicks_per_campaign_and_time", methods=['GET'])
@auth.auth_required
async def get_clicks_per_campaign_and_time(request):

    try:

        campaign = int(request.form['campaign'].pop(0))
        time_from = int(datetime.datetime.strptime(request.form['start_date'].pop(0), "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.UTC).timestamp())
        time_to = int(datetime.datetime.strptime(request.form['end_date'].pop(0), "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.UTC).timestamp())
        query = "select count(id) from click_log where campaign='{0}' and timestamp >= {1} and timestamp < {2};".format(
            campaign,
            time_from,
            time_to
        )
        result = await mysql_service.get_data_for_api(query)

        return json({"Clicks": result[0]})

    except Exception as e:

        return json({"Message": e})


@app.listener('before_server_start')
async def init_server(app, loop):
    tracemalloc.start()


def run():
    logger.info("Starting server")
    app.run(host='0.0.0.0', port=9002, debug=settings.DEBUG)
