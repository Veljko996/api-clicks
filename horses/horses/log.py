import datetime

from sanic.log import LOGGING_CONFIG_DEFAULTS

from horses import settings


CONFIG = {
    **LOGGING_CONFIG_DEFAULTS,

    'formatters': {
        **LOGGING_CONFIG_DEFAULTS['formatters'],
        'default': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        }
    },

    'handlers': {

        **LOGGING_CONFIG_DEFAULTS['handlers'],
        'file': {
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': 'logs/log',
            'when': 'D',  # S,M,H,D,W0-W6 (Weekday 0=Monday),midnight
            'interval': 1,
            # If backupCount is nonzero, at most backupCount files will be kept,
            # and if more would be created when rollover occurs,
            # the oldest one is deleted.
            # The deletion logic uses the interval to determine which files to delete,
            # so changing the interval may leave old files lying around.
            'backupCount': 7,
            'encoding': None,  # If encoding is not None,
                               # it is used to open the file with that encoding.
            'delay': None,  # If delay is true, then file opening is deferred
                            # until the first call to emit().
            # If atTime is not None, it must be a datetime.time instance
            # which specifies the time of day when rollover occurs, for the cases
            # where rollover is set to happen “at midnight” or “on a particular
            # weekday”. Note that in these cases, the atTime value is effectively
            # used to compute the initial rollover, and subsequent rollovers would
            # be calculated via the normal interval calculation.
            'atTime': datetime.time(0, 0, 0, 0),
            'formatter': 'default',
        },

    },

    'loggers': {
        **LOGGING_CONFIG_DEFAULTS['loggers'],
        'horses': {
            'level': settings.LOG_LEVEL,
            'handlers': ['console', 'file'],
        }
    }
}
