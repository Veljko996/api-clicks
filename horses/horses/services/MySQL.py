import logging
from socket import gaierror

import aiohttp
from functools import partial
from yarl import URL
import asyncio
import aiomysql

from horses import settings

logger = logging.getLogger(__name__)


class MySQL:
    def __init__(self, host, port, user, password, db):
        self.pool = None
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.db = db
        logger.info(f"Host: {self.host}, port: {self.port}, user: {self.user}, pass: {self.password}, db: {self.db}")

    async def get_data_for_api(self, query):
        self.pool = await aiomysql.create_pool(host=self.host, port=self.port,
                                               user=self.user, password=self.password,
                                               db=self.db)
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(query)
                (r,) = await cur.fetchall()
                return r
        conn.close()


mysql_service = MySQL(settings.MYSQL_HOST, settings.MYSQL_PORT, settings.MYSQL_USER,
                      settings.MYSQL_PASSWORD, settings.MYSQL_DATABASE)

