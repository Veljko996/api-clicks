import os
import json
import decimal


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


class Envs:

    envs = None

    def getenv(self, key, default=None, convert_to_bool=False):
        if self.envs is None:
            self.load_envs()

        value = os.getenv(key, self.envs.get(key, default))

        if convert_to_bool:
            value = str2bool(value)

        return value

    def load_envs(self):
        path = os.getenv('BASE_PATH', '')
        with open(os.path.join(path, 'environment'), 'r') as env_file:
            text = ''
            for line in env_file:
                text += line

            text = text.replace('\'', '"')  # Replace single quote with double
            text = text.replace('\n', '')  # Drop \n

            self.envs = json.loads(text)


envs = Envs()

