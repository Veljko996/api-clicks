import pytest

from horses.server import app as _app


@pytest.yield_fixture
def app():
    yield _app


@pytest.fixture
def client(loop, app, sanic_client):
    return loop.run_until_complete(sanic_client(app))
