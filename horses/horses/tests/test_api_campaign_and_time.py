import json
import os
import copy

import pytest

from horses.services import mysql_service

from horses.tests.test_client import *


def load_data(model_resource_dir):
    parent_dir = os.path.dirname(os.path.abspath(__file__))
    model_abspath = f"{parent_dir}/{model_resource_dir}"

    data = json.load(open(f"{model_abspath}/request.json"))
    expected = json.load(open(f"{model_abspath}/response.json"))
    mocked_data = json.load(open(f"{model_abspath}/campaign_and_time.json"))

    return data, expected, mocked_data


def mock_external_services(mocker, mocked, campaign):
    async def test_get_data_for_api(query):
        return (mocked[str(campaign)],)

    mocker.patch.object(mysql_service, "get_data_for_api", side_effect=test_get_data_for_api)


@pytest.mark.parametrize("model_resource_dir", [
    "campaign_and_time"
])
async def test_api_campaign_and_time(mocker, client, model_resource_dir):
    data, expected, mocked = load_data(model_resource_dir)

    mock_external_services(mocker, mocked, data["campaign"])

    response = await client.get("/get_clicks_per_campaign_and_time",
                                data=data,
                                headers={
                                    'X-My-App-Auth-Token': 'veljkow3*5lm1rc2axu^sx4h80f%9#+@90r&eq3y-_wxb(f=l+rpr'})

    actual = await response.json()
    assert response.status == 200

    # test expected and actual dict keys
    assert expected.keys() == actual.keys()

    for key in expected.keys():
        # test number of clicks
        assert expected[key] == actual[key]